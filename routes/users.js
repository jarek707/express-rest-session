let callController = require('../models/util.js').callController;

module.exports = {
    getRoutes :     callController, //ROUTER ['get', 'getRoutes', '', 'SU']
    all :           callController, //ROUTER ['GET', 'allUsers',  '', 'SU']

    _get:    callController, //ROUTER ['get',    'getUser', '/:uid([0-9\\-a-f]+$)']
    _delete: callController, //ROUTER ['delete', 'delUser', '/:uid([0-9\\-a-f]+$)', 'ADM']
    _put:    callController, //ROUTER ['put',    'upsertUser',   '', 'ADM']
    _post:   callController  //ROUTER ['post',   'upsertUser',   '', 'ADM']
}
