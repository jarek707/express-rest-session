## Express-Rest-Session
Simple REST API with PostgreSQL supoport using node Express.  It supports sessions with `express-session` module.

## ___Server (quick setup)___

#### 1. Start with
```sh
git clone git@gitlab.com:jarek707/express-rest-session.git
cd express-pg-api
npm install
```

#### 2. Edit file `config.json.sample`, replace `<param>` with values corresponding to your environment
```js
{
    "appDatabase": "<mySql|pgSql>", // replace with "mySql" or "pgSql"
    "dbUrl": "postgres://<postgresAccountName>:<postgresAccountPassword>@<server>/<databaseName>",
    "mySql": {
        "host": "localhost",
        "user": "root",
        "password" : "",
        "database" : "<Database Name>"
    },
    "srvPort": "<Server Port Number|8082>", // npm run browser opens http://localhost:8082
    ...
}
```
and save the file as **`config.json`**

#### 3. You are now ready to start the server

```sh
$ npm start
```
or 
```sh
$ node server.js
```
To run with browser interface
```sh
$ npm run browser
```

***
## ___Defining route controllers___
[https://gitlab.com/jarek707/express-rest-session/tree/master/routes]

***
## ___Configure gMail (optional)___
[https://gitlab.com/jarek707/express-rest-session/tree/master/ctrls]
