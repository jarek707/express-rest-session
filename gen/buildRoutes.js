let fs        = require('fs'),
    routesDir = './routes';

let singleRoute =`
        app.__METHOD('/__ROUTE__ENDPOINT__PARAMS', init.authCheck,
            (req, res) => {
                require('${routesDir}/__ROUTE.js').__CALLENDPOINT(
                    '__ROUTE', '__ACTION', req.body, res, response => output(res, response)
                )
            }
        );`;

// OUTPUT HEAD START
let out = `var init = require(\'./init.js\');

let output = (res, response) => {
    res.end( typeof response == 'string' ? response : JSON.stringify(response) );
}

module.exports = {
    setRoutes: app => {
`;
// OUTPUT HEAD END

let buildRoutesTable = () => {
    var out = {};

    fs.readdirSync(routesDir) // router/*.js
    .map( f => {
        if (f.match( /.*\.js$/)) {

            var routeLines = fs.readFileSync( `${routesDir}/` + f , 'utf8').split('\n')
                   .filter( fl => fl.indexOf('ROUTER') > -1)
                    .map( fl => {
                        try {   // Catch it when somebody puts in a word ROUTER in router.js without
                                // the corresponding route definition array
                            let path = JSON.parse(fl.replace(/^.*ROUTER[\s]*/, '').replace(/'/g, '"'))
                            path.unshift( fl.split(':')[0].trim() ); // prepend router endpoint name

                            //if (!path[path.length-1])
                                //path[path.length-1] = 'REG';

                            return path;
                        } catch (e) {
                            return null;
                        }
                    }).filter( i => i );

            out[f.replace(/\.js/g, '')] = routeLines;
        }
    })
    return out;
}

let routesTable  = buildRoutesTable();
fs.writeFileSync( './gen/routesTable.json', JSON.stringify(routesTable, null, 4) );

module.exports = {
    generateExpressRoutes: () => new Promise( (y, n) => {
        if ( global.appConfig && global.appConfig.skipRouteGeneration ){
            y();
        } else {
            for (var i in routesTable)
                routesTable[i].forEach( dd => {
                    let endPoint = dd[0];

                    if (['_get', '_post', '_put', '_delete'].indexOf( endPoint.toLowerCase() ) > -1)
                        endPoint = '';
                    else 
                        endPoint = '/' + endPoint;

    console.log( endPoint, dd, i );
                    out += singleRoute.replace(/__METHOD/,  dd[1].toLowerCase())
                                      .replace(/__ROUTE/g,         i)
                                      .replace(/__ENDPOINT/g, endPoint)
                                      .replace(/__CALLENDPOINT/g, dd[0])
                                      .replace(/__ACTION/g,   dd[2])
                                      .replace(/__PARAMS/,  typeof dd[3] == 'undefined' ? '' : dd[3]) + '\n'
                });

            out += '\t}\n}';
            fs.writeFileSync('./generatedRoutes.js', out);
            y();
        }
    })
}

console.log( typeof process.argv[2] == 'undefined' ? '...Server Startup...'
                                                   : '...GENERATING ROUTES...');

typeof process.argv[2] == 'undefined' || module.exports.generateExpressRoutes();

process.on('uncaughtException', err =>
    console.log('UNCAUGHT when generating Routes\n', err )
);
