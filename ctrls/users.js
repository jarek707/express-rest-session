const   db          = global.appConfig.dbModule,
        dbSchema    = require('../models/dbSchema.js'),
        buildUpsert = require('../models/dbSchema.js').buildUpsert,

        util         = require('../models/util.js'),
        returnResult = util.returnResult // returnResult( data, success, additionalMessage )
        ;

const me = module.exports = {

        getUser: (params, cb) => {
            let sql  = 'SELECT uid, login, first, last, perms, email FROM users WHERE uid=$1';
            let data = [params.uid];

            db.promiseQuery(sql, data, result => result[0]) // there should only be one row
                .then( response => {
                    cb( returnResult(response) );
                })
                .catch( err => {
                    cb( returnResult(err, false), 500);
                });
        },

        upsertUser: (params, cb) => {
            if ( !params.login && !params.email) {
                cb( returnResult(null, false, `Login or email is required`), 401);
                return;
            }

            if (global.appConfig.perms.indexOf( params.perms ) == -1) {
                params.perms = 'REG'; // if perms are missing or invalid set it to REG
            }

            //
            //  Allow only SU and ADM to create and update users
            //  ADM cannot create/update SU
            //  other users can only update their own account
            //

            if  (params.sessionUserPerms != 'SU' 
                    || params.sessionUserPerms == 'ADM'      && params.perms == 'SU'
                    || params.sessionUserUid   != params.uid && params.perms == 'REG'
                ) {
                        cb( `Not authorized to create user with permissions ${params.perms}`, 401);
            } else {
                let {sql, data} = buildUpsert(params, 'users');

                db.promiseQuery(sql, data)
                    .then( response => {
                        cb( returnResult(response));
                    })
                    .catch( err => {
                        cb( returnResult(err, false, 'UPSERT ERROR'), 500 );
                    })
                    ;
            }
        },

        delUser: (params, cb) => {
            db.promiseQuery(`DELETE FROM users where uid=$1`, [params.uid] )
                .then( resp => {
                    cb( returnResult(null, true, 'User Deleted') );
                })
                .catch( err => {
                    cb( returnResult(err, false, 'Delete User Error'), 500 );
                })
                ;
        },

        allUsers: function(params, cb) {
            var sql = "SELECT uid, first, last, email, perms, login FROM users";
            var data = [];

            db.promiseQuery(sql, data)
                .then( allUsers => {
                    cb( returnResult(allUsers));
                })
                .catch( err => {
                    cb( returnResult(err, false, 'GET ALL Users Error'), 500 );
                })
                ;
        },

        getRoutes: (params, cb) => {
            cb( returnResult(global.appConfig.routesTable) );
        }
};
