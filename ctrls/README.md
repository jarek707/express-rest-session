## _Defining route controllers_
**Sample controller file `infra.js`**

```sh
let mod = require('.' + global.appConfig.modDir + 'infra.js');

let output =  (q, cb, modMethod) => mod[modMethod](q, r => cb( r ));

module.exports = {
    heartbeat:      output, //ROUTER ['get', 'heartbeat', '', 'PUB']
    getUser:        output, //ROUTER ['get', 'getUser', '/:uid', 'SU']
    getUsers:       output, //ROUTER ['get', 'getUsers', '', 'PUB']
    saveUser:       output, //ROUTER ['post', 'saveUser', '', 'PUB']
    delUser:        output, //ROUTER ['get', 'delUser', '', 'ADM']
    changePasswd:   output, //ROUTER ['post', 'changePasswd', '']
}
```

In the above file `infra.js` routes are constructed from the name of the file 
and the `key` in `module.exports` object (eg. **infra/saveUser**).

For example, a route **`http(s)://<Your Server Url>/infra/heartbeat`** would correspond to the first entry.

Specific parameters of the route are defined in the comment following **`//ROUTER `**

For entry `getUser` the route **`['get', 'getUser', '/:uid', 'SU]`** corresponding to the endpoint

**`http(s)://<Your Server Url>/infra/getUsers/e9e9af99-8f82-9b62-4483-fd9b5ce40760`**

is defined as follows

 METHOD | Model method called | REST params | Permissions
 ------ | ------------------- | ----------- | -----------
 get    | getUser             | e9e9af99-8f82-9b62-4483-fd9b5ce40760       | SU
 
 
 
 **Permissions**
 
 Permissions are hierarchical. They observe the following order: **`SU, ADM, REG, PUB`**, 
 where  a user with permissions **`SU`** can access all endpoints,
 **`ADM`** can access all endpoints except for **`SU`**, etc.
 
 Leaving permission blank (empty string) defaults to **`REG`**, which requires a valid login.
 
 Permission **`PUB`** is the only one that does not require a valid login.
 ***
