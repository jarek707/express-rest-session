let db,
    url = require('url');

if (global.appConfig.appDatabase == 'mySql') {
    db  = require(`../models/myDb.js`);
} else {
    db  = require(`../models/pgDb.js`);
}

let returnResponse = function(resp, action, success, msg, data, user_uid, puid, name , login, puid) {
    resp.end(JSON.stringify({
            "action":   action,
            "success":  success,
            "msg" :     msg || '',
            "data" :    data,
            "user_uid": user_uid,
            "name" :    name,
            "login":    login
    }));
};

function verifiedEmailLoginDb(email, cb) {
    var sql = "SELECT * FROM users WHERE email=$1";
    let data = [email];

    //if (global.appConfig.appDatabase == 'mySql')
        //sql = sql.replace(/\$\d/g, '?');

    db.promiseQuery(sql ,data)
    .then( response => cb( response[0] ))
    .catch( e => cb( null ) );
}

function userLoginDb(login, passwd, cb) {
    let sql  = "SELECT * FROM users WHERE login=$1 AND passwd=$2";
    let data = [login, passwd];

    //if (global.appConfig.appDatabase == 'mySql')
        //sql = sql.replace(/\$\d/g, '?');

    db.promiseQuery(sql ,data)
    .then( response => {
        cb( response[0] )
     })
    .catch( e => {
        console.log( e , 'err ');
        cb( null )
     });
}

let googleClientId;

try { // Safety, in case google section was deleted from config.json during install
    googleClientId = global.appConfig.gmailCreds.installed.client_id;
} catch (e) {
    googleClientId = '<CLIENT_ID>';
}

let shouldCheckGoogle = q => {
    return
        q.email && (q.email.indexOf('gmail.com') > -1 || q.email.indexOf( 'google' ) > -1)
            && googleClientId.indexOf('<') == -1 && googleClientId.indexOf( 'CLIENT_ID' ) == -1
}

//
// Call Google API to verify login token
// this token s  returned from google login in the browser
//
let checkGoogleLogin = q => new Promise( (y, n) => {
    var token = q.token;

    var url = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=";

    require('request')(url + token, function(err, rez, body) {
        body = JSON.parse(body);

        if (body.issued_to == googleClientId) //oauth2ClientId
            y( body )
        else
            n( null )
    });
});

let setSessionUser = (req, loginResponse) => {
    req.session.user = {
        login:      loginResponse.login,
        email:      loginResponse.email,
        perms:      loginResponse.perms || 'REG',
        user_uid:   loginResponse.uid
    };
    req.session.cookie.maxAge = 600000;
}

module.exports = {
    logout: (req, resp, cb) => {
        req.session.user = null;
        resp.end(JSON.stringify({success: true}));
    },
    login: (req, resp, cb) => {
        let q = req.params;

        if (q.login || q.email) {

            if ( shouldCheckGoogle(q) ) {
                checkGoogleLogin(q)
                .then( googleResp => {
                    if (googleResp.verified_email) {
                        verifiedEmailLoginDb(q.email, loginResponse => {
                            if (loginRes.length) {
                                setSessionUser( req, loginResponse );
                                typeof cb == 'function' && cb({"success": true, "data":  loginResponse});
                            } else {
                                req.session.user = null;
                                cb({ "success": false, "msg": "User not found"});
                            }

                        });
                    } else {
                        returnResponse(resp, "login", false, 'Goggle Email valid but not found in database')
                    }
                });
            } else {
                userLoginDb(q.login, q.passwd, function(loginResponse) {


                    if (loginResponse) {
                        setSessionUser( req, loginResponse );
                        typeof cb == 'function' && cb({"success": true, "data":  loginResponse});
                    } else {
                        req.session.user = null;
                        cb({ "success": false, "msg": "User/password not found" });
                    }

                    req.session.save( err => {
                        err && console.log( 'Session save err:', err );
                    })
                });
            }
        } else {
            returnResponse(resp, "login", false, 'User Name Or Email required')
        }
    }
}
