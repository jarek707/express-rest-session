let app        = null,  // this is express defined in srv.js
    authRoutes = {};    // routes table (with route permissions)

global.appConfig = require('./config.json')

if (global.appConfig.appDatabase == 'mySql') {
    global.appConfig.dbModule = require('./models/myDb.js');
} else {
    global.appConfig.dbModule = require('./models/pgDb.js');
}

// TODO remove after removing db references from util.js
require('./models/util.js').setDb( global.appConfig.dbModule );

//
// Generate appRoutes file
// Setup Permissions in authRoutes object {endpoint: SU|ADM|REG|PUB}
//
let buildRoutes = () => {
    require('./gen/buildRoutes.js').generateExpressRoutes()  // generate generatedRoutes.js file
    .then( () => {
        require('./generatedRoutes.js').setRoutes(app);      // Attatch routes to express

        var routesTable = require('./gen/routesTable.json'); // Read routesTable table object
        global.appConfig.routesTable = routesTable;

        for (var i in routesTable)
            routesTable[i].length && routesTable[i].forEach( // Setup permissions for each route
                rr => authRoutes[(i + '/' + rr[0]).replace(/^\//, '')] = rr[4] || 'REG'
            );
    })
    .catch( e => console.log( `Error: Can't find generatedRoutes.js file` ));
}

//
let returnUnauthorized = (req, res, code = 401) => {
    req.session.user = null;
    res.statusCode = code;

    if (code == 500) {
        res.end(`Backend API Error while resolving permissions`);
    } else {
        res.end(`Insuficient Permissions\n\nPlease login with a privileged account`);
    }
}

module.exports = {

    //
    // called from server.js to pass on express app reference and build routes for express
    //
    setApp: inApp => {
        buildRoutes( app = inApp )
    },

    //
    // authCheck -  Middleware for authenticating permissions
    //              Compares route permission to the user's access permisions
    //              If the user has access it calls setParams()
    //              to combine GET, POST, PUT and DELETE params into req.params
    //
    authCheck: (req, res, next) => {
        // get the path key
        let path  = require('url').parse(req.url, true).pathname.split('/'),
            route = [path[1], path[2]].join('/').replace(/\/$/, ''),

            // Perms are ordered by access priority 'SU', 'ADM', 'REG', 'PUB'
            allPerms   = ['SU', 'ADM', 'REG', 'PUB'],
            routePerms = authRoutes[route]   && authRoutes[route].trim()    || 'REG'
            userPerms  = req.session.user && req.session.user.perms.trim()  || 'PUB';

        if ( req.session.user || routePerms == 'PUB') {
            let sufficientPerms = allPerms.indexOf(userPerms) <= allPerms.indexOf(routePerms);

            if (sufficientPerms || routePerms == 'PUB') {

                try {
                    Object.assign( req.body, req.params);
                    req.body.sessionUserUid   = req.session.user.user_uid;
                    req.body.sessionUserPerms = req.session.user.perms;
                    next();
                }
                catch (e) {
                    returnUnauthorized(req, res, 500); // Error while setting Params
                };
            } else {
                returnUnauthorized(req, res); // user logged in but doesn't have sufficient Perms
            }
        } else {
            returnUnauthorized(req, res); // user not logged in and route is not Public
        }
    },

    //
    setHeaders: res => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
        res.setHeader("Content-Type", "application/json; charset=utf-8");
    }
}
