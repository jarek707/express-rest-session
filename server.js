'use strict';

let express = require('express'),
    app     = express(),
    init    = require('./init.js'),
    bodyParser = require('body-parser')
    ;

let session   = require('express-session');

process.setMaxListeners(200);

app.set('trust proxy', 1) // trust first proxy

if (global.appConfig.appDatabase == 'mySql') {
    let MySQLStore = require('express-mysql-session')(session);
    let sessionStore = new MySQLStore(global.appConfig.mySql);
     
    app.use(session({
        key: 'session_cookie_name',
        secret: 'session_cookie_secret',
        store: sessionStore,
        resave: false,
        saveUninitialized: false
    }));
} else {
    let pgSession = require('connect-pg-simple')(session);
    app.use(session({
        store: new pgSession({
            conString: global.appConfig.dbUrl
        }),
        secret: 'keyboard cat',
        resave: false,
        maxAge: 1000 * 600,
        saveUninitialized: true,
        cookie: { secure: false }
    }));
}

// 
// Register app with init and generate express route table (generatedRoutes.js)
//
init.setApp( app );

//app.use( bodyParser.json() );
//app.use( bodyParser.text() );
//app.use( bodyParser.raw() );
app.use(bodyParser.urlencoded({ extended: false }));

//
// Login and Logout endpoints need be defined outside of generated routing
//
app.get('/auth/login/:login/:passwd', (req, res) => {
    require('./ctrls/auth').login(req, res, resp => {
        res.statusCode = resp.success ? 200 : 401;
        res.end( JSON.stringify(resp) );
    })
});

app.get('/auth/logout', (req, res) => {
    require('./ctrls/auth').logout(req, res, resp => {
        res.end('{"success": true, "msg": "logged out"}')
    })
});

app.get('/heartbeat', (req, res) => {
    let resp = req.session.user || {};

    resp.appDatabase = global.appConfig.appDatabase;
    res.statusCode = 200;

    res.end( JSON.stringify( resp ) );
});

app.use(express.static('public'));

//
// Run Server
//
app.listen(global.appConfig.srvPort);
require('./models/util.js').blog(global.appConfig.srvPort, 'SERVER RUNNING ON PORT');

require('fs').writeFileSync( './.killLastInstance', `kill ${process.pid}` );
