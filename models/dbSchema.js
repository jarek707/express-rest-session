var UT = require('./util.js');

let isUuid = function(uid) {
    return  uid.toString().match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i);
};

const TXT = 0;
const UID = 1;
const INT = 2;
const UPD = 3;
const BOOL= 4;
const JSN = 5;
const DAT = 6;
const TIM = 7;
const CHR = 8;
const NUM = 9;

var fields = {
    'users' : {
        first       : CHR,
        last        : CHR,
        phone       : CHR,
        email       : CHR,
        passwd      : CHR,
        login       : CHR,
        perms       : CHR
    }
};

module.exports = {
    createQuery: function(key) {
        let sql = `CREATE TABLE ${key} ( uid uuid not null default uuid_in((md5(((random())::text || (now())::text)))::cstring)`;
        var f = fields[key];
        for (var i in f) {
            switch (f[i]) {
                case CHR:  sql += ',\n\t' + i + ' varchar(128)'; break;
                case TXT:  sql += ',\n\t' + i + ' text'; break;
                case UID:  sql += ',\n\t' + i + ' uuid'; break;
                case INT:  sql += ',\n\t' + i + ' integer'; break;
                case BOOL: sql += ',\n\t' + i + ' boolean'; break;
                case DAT:  sql += ',\n\t' + i + ' date'; break;
                case JSN:  sql += ',\n\t' + i + ' text'; break;
                case TIM:  sql += ',\n\t' + i + ' timestamp'; break;
            }
        }
        sql = sql + ');';
        return sql;
    },
    //
    // buildUpsert -    
    //
    buildUpsert: function(q, key) {
        var f = fields[key];

        var isUpdate = typeof q.uid != 'undefined' && q.uid;
        isUpdate || (f.created = UPD);

        var ret = {vals: '', flds: '', data: [], warn:[], type: []};
        var idx = 1;
        for (var i in f) {
            if (typeof q[i] != 'undefined') { // ignore missing values
                ret.type.push(f[i]);

                if (isUpdate) {
                    ret.flds += ', ' + i + '=$' + idx + (f[i] == UID ? '::uuid' : '');
                } else {
                    ret.flds += ', ' + i;
                    ret.vals += ', $' + idx + (f[i] == UID ? '::uuid' : '');
                }

                if (f[i] == DAT) {
                    var matched = q[i].match(/^[1-9]{4}-[0-9]{2}-[0-9]{2}$/g);
                }
                    
                ret.data.push(f[i] === INT 
                    ? parseInt(q[i])
                    : (f[i] === JSN ? JSON.stringify(q[i]) : q[i]));
                idx++;
            }
        }

        if (isUpdate) {
            ret.flds += ', updated=now()';
        } else {
            if (ret.flds.indexOf('created') == -1) {
                ret.flds += ', created ';
                ret.vals += ', now()';
            }
        }

        ret.flds = ret.flds.substring(2);
        ret.vals = ret.vals.substring(2);
        ret.last = '$' + idx;

        var sql = '';
        if (isUpdate) {
            ret.data.push(q.uid);
            //sql =  "UPDATE " + key + " SET " + ret.flds + " WHERE uid=$" + idx + "::uuid ";
            if (global.appConfig.appDatabase == 'pgSql') {
                sql =  "UPDATE " + key + " SET " + ret.flds + " WHERE uid=$" + q.uid + "::uuid ";
            } else {
                sql =  "UPDATE " + key + " SET " + ret.flds + " WHERE uid=" + q.uid;
            }
        } else {
            sql =  "INSERT INTO " + key +  " (" + ret.flds + ") VALUES (" + ret.vals + ")";
        }

        if (global.appConfig.appDatabase == 'pgSql') {
            sql += " RETURNING uid";
        }

        ret.sql = sql;

        return ret;
    },
    debug: function(q, key) {
        let upsertQ = module.exports.genUpsert( q, key );
        let sql     = upsertQ.sql;

        upsertQ.data.map( (l, i) => {
            let quotes = [INT, BOOL].indexOf(upsertQ.type[i]) == -1 ? "'" : '';
            sql = sql.replace( `$${i+1}`, `${quotes}${l}${quotes}`);
        });

        return sql;
    }
}
