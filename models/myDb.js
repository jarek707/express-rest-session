let UT  = require('./util.js');

let myDb = require('mysql').createConnection(
    global.appConfig.mySql
);

myDb.connect();

let getConn = cb => cb(myDb);

let retries = 9;

let resp = (data, success, msg) => {
    typeof msg == 'undefined' && (msg = '');
    success || UT.log.err('DB ERROR', data);
    return {"data": data, "success": success, "msg": msg};
}

let me;
module.exports = me = {
    //
    // Functions query and promiseQuery
    // Arguments:
    //      sql   - sql query with field valud replacements ($1, $2, ...)
    //      data  - array of corresponding values
    //      cb - is the standard callback function returning result rows in (query only)
    //
    //      midCb - optional middleware callback
    //              it accepts query rows and returns them in the same format after processing
    //
    query: (sql, data, cb, midCb = null) => {
        sql = sql.replace(/\$\d+/g, '?'); //convert postges call to mySql

        myDb.query(sql, data, (e, r) => {
            console.log( r );
            let result = typeof midCb == 'function' ? midCb( r ) : r;
            e || typeof cb == 'function' && cb(result);
        })
    },
    promiseQuery: function(sql, data, midCb) { return  new Promise( (y, n) => {
        sql = sql.replace(/\$\d+/g, '?'); //convert postges call to mySql

        myDb.query(sql, data, (e,r) => {
            if (e) {
                n( e );
            } else {
                let result = typeof midCb == 'function' ? midCb(r) : r;
                y( result );
            }
        })
    })}
}

//
// INITIALIZATION
//
// Start database connection and validate required tables
//
let usersTableName = 'users';
let defaultCheck =  {

    findTablesSql: `
        SELECT table_name
        FROM information_schema.tables
        WHERE table_schema = '${global.appConfig.mySql.database}'
            AND (table_name = 'sessions' OR table_name ='${usersTableName}')`,

    createUsersTableSql: `
        create table ${usersTableName} (
            uid int primary key not null auto_increment,
            login   varchar(255),
            passwd  varchar(255),
            auth    varchar(4),
            perms   varchar(4),
            first   varchar(32),
            last    varchar(32),
            email   varchar(255),
            updated timestamp DEFAULT CURRENT_TIMESTAMP,
            created timestamp DEFAULT CURRENT_TIMESTAMP
        )`,

    createSessionTableSql: `
        CREATE TABLE IF NOT EXISTS \`sessions\` (
            session_id varchar(128) COLLATE utf8mb4_bin NOT NULL,
            expires int(11) unsigned NOT NULL,
            data text COLLATE utf8mb4_bin,
            PRIMARY KEY (session_id)
        ) ENGINE=InnoDB`
}

//
// Initialize connection and run a quick test to see if users and session tables exist
//
function createFirstAdminUser() {
    me.promiseQuery(`SELECT COUNT(*) AS count FROM users WHERE login='admin'`)
    .then( response => {
        if (parseInt(response[0].count) == 0)
            myDb.query(`INSERT INTO ${usersTableName} 
                (login, passwd, first, last, perms) VALUES (?, ?, ?, ?, ?)`,
                ['admin', 'change-me', 'admin', 'Admin', 'SU']);
    });
}


getConn( () => {
    myDb.query(defaultCheck.findTablesSql, [], (e,r) => {
        console.log( r, ' r ' );
        console.log( e, ' e ' );
        if (e) {
            console.log( 'error:', e );
        } else {
            let found = r.filter( t => [usersTableName, 'sessions'].indexOf(t.table_name) > -1)
                         .map(    t => t.table_name );

            if (found.indexOf( usersTableName  ) > -1) {
                UT.glog( 'OK', `TABLE ${usersTableName} -` );
                createFirstAdminUser();
            } else {
                UT.brlog('Creating table ' + usersTableName);
                myDb.query(defaultCheck.createUsersTableSql, [], (e,r) => {
                    if (e) {
                        console.log( e );
                        UT.rlog(`Create Table ${usersTableName} users Error`);
                    } else {
                        UT.glog('Table users created - OK');
                        createFirstAdminUser();
                    }
                });
            }

            if (found.indexOf( 'sessions' ) > -1) {
                UT.glog( 'OK', 'TABLE sessions -' );
            } else {
                UT.brlog('Creating table sessions');
                myDb.query(defaultCheck.createSessionTableSql, [], (e,r) => {
                    if (e) {
                        console.log( e );
                        UT.rlog('Create Table sessions Error');
                    } else {
                        UT.glog('Table sessions created - OK');
                    }
                });
            }
        }
    });
});
