let pg  = require('pg'),
    UT  = require('./util.js');

let retries = 9;
let myDb    = null;

let restartConn = () => setTimeout( getConn, 200 );

let getConn = (cb) => {
    if (myDb == null) {
        let connUrl = global.appConfig.dbUrl;

        new pg.Client(connUrl)
        .connect( (er, con) => {
            if (er) {
                UT.brlog( 'DB connUrl:' + connUrl, er.toString() + ' ' + retries );
                if ( retries-- > 0) {
                    restartConn();
                } else {
                    UT.rlog('Connection not established', 'EXITING after 10 connection attempts');
                    process.exit();
                }
            } else {
                myDb = con;
                UT.glog( connUrl,'DB CONNECTION: ' );
                typeof cb == 'function' && cb();
            }
        });
    }
};

let me;
module.exports = me = {
    //
    // Functions query and promiseQuery
    // Arguments:
    //      sql   - sql query with field valud replacements ($1, $2, ...)
    //      data  - array of corresponding values
    //      cb - is the standard callback function returning result rows in (query only)
    //
    //      midCb - (optional, middleware-like) callback
    //              it accepts query rows and returns result after processing
    //
    query: (sql, data, cb, midCb = null) => {
        myDb.query(sql, data, (e,r) => {
            let result = typeof midCb == 'function' ? midCb( r.rows ) : r.rows;
            e || typeof cb == 'function' && cb(result);
        })
    },
    promiseQuery: function(sql, data, midCb = null) { 
        return new Promise( (y, n) => {
            myDb.query(sql, data, (e,r) => {
                if (e) {
                    n( e );
                } else {
                    let result = typeof midCb == 'function' ? midCb( r.rows ) : r.rows;
                    y( result );
                }
            })
        })
    },
    transaction: (sql, cb) => {
        myDb.query('BEGIN;' + sql, [], transResult => {
            myDb.query('COMMIT', [],    ()  => cb({success: true,  result: transResult}),
                                        err => cb({success: false, msg: err}) // TODO need to rollback
            )
        }, err => {
            myDb.query('ROLLBACK', [],  () => cb({success: false, msg: err}),
                                        () => cb({success: false, msg: err})
            )
        });
    }
}

//
// INITIALIZATION
//
// Start database connection and validate required tables
//
let usersTableName = 'xusers';
let defaultCheck =  {

    findTablesSql: `
        SELECT table_name
            FROM information_schema.tables
            WHERE
              table_name = 'session' OR table_name ='${usersTableName}'`,

    createUsersTableSql: `
        CREATE TABLE ${usersTableName} (
            uid UUID default (md5(((random())::text || (clock_timestamp())::text)))::uuid,
            login  VARCHAR(32),
            passwd VARCHAR(255),
            auth   VARCHAR(4),
            perms  VARCHAR(4),
            first  VARCHAR(32),
            last   VARCHAR(32),
            email  VARCHAR(32)
        )`,

    createSessionTableSql: `
        CREATE TABLE "session" (
            "sid" varchar NOT NULL COLLATE "default",
            "sess" json NOT NULL,
            "expire" timestamp(6) NOT NULL
        )
        WITH (OIDS=FALSE);
        ALTER TABLE "session" ADD CONSTRAINT "session_pkey" PRIMARY KEY ("sid") NOT DEFERRABLE INITIALLY IMMEDIATE`
}

//
// Initialize connection and run a quick test to see if users and session tables exist
//
getConn( () => {
    myDb.query(defaultCheck.findTablesSql, [], (e,r) => {
        if (e) {
        } else {
            let found = r.rows.filter( t => [usersTableName, 'session'].indexOf(t.table_name) > -1)
                              .map(    t => t.table_name );

            if (found.indexOf( usersTableName  ) > -1) {
                UT.glog( 'OK', `TABLE ${usersTableName} -` );
            } else {
                UT.brlog('Creating table ' + usersTableName);
                myDb.query(defaultCheck.createUsersTableSql, [], (e,r) => {
                    if (e) {
                        UT.glog(`Create Table ${usersTableName} users Error`);
                    } else {
                        UT.glog('Table users created - OK');
                    }
                });
            }

            me.promiseQuery(`SELECT COUNT(*) FROM users WHERE login='admin'`, [])
            .then( resp => {
            console.log( resp, ' REsP to find amdin ' );
                /*
                myDb.query(`INSERT INTO ${usersTableName} 
                    (login, passwd, first, last, perms) VALUES ($1, $2, $3, $4, $5)`,
                    ['admin', 'change-me', 'admin', 'admin', 'SU']);
                    */
            });

            if (found.indexOf( 'session' ) > -1) {
                UT.glog( 'OK', 'TABLE session -' );
            } else {
                UT.brlog('Creating table sessions');
                myDb.query(defaultCheck.createSessionTableSql, [], (e,r) => {
                    if (e) {
                        UT.glog('Create Table session Error');
                    } else {
                        UT.glog('Table session created - OK');
                    }
                });
            }
        }
    });
});
