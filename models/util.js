var c = {
    black           :'\x1b[30m',
    blue            :'\x1b[34m',
    green           :'\x1b[32m',
    cyan            :'\x1b[36m',
    red             :'\x1b[31m',
    purple          :'\x1b[35m',
    brown           :'\x1b[33m',
    gray            :'\x1b[37m',
    end             :'\x1b[0m'
};

function logHeart(arg) {
    try {
        require('fs').appendFile('./log/heartbeat.log', c.gray + arg + c.end + '\n');
    } catch (e) {
        console.log(c.red +  e.stack + c.end);
    }
}
function doLog(arg, color) {
    try {
        //console.log(color, JSON.stringify(Array.from(arg).join(), null, 4), '\x1b[0m');
        console.log(color, JSON.stringify(arg, null, 4), '\x1b[0m');
    } catch (e) {
        console.log(c.red +  e.stack + c.end);
    }
}

function resp(data, success, msg) {
    success || console.log( c.red + 'DB ERROR', data, c.end);
    return {"data": data, "success": success, "msg": msg};
}


let db = null;

var me = {
    setDb: inDb => db = inDb,

    deep: function(arg)        { return JSON.parse(JSON.stringify(arg)); },
    isAdm :    function(perms) { return perms.substring(0,3) === 'ADM'; },
    isReg :    function(perms) { return perms.substring(0,3) === 'REG'; },
    isClient : function(perms) { return perms.substring(0,3) === 'CLI'; },

    parseRequest: function(r, data) {
        var url = require('url');
        var q   = data ? data : url.parse(r.url, true).query;
        var p   = url.parse(r.url, true).pathname.split('/');

        p[0]   == '' && p.shift(); 
        return {p: p, q: q, h: r.headers}
    },

    getColors: function() { return c; },
    c: c,
    colors:  c,
    ucfirst: function(inStr) {
        return inStr.charAt(0).toUpperCase() + inStr.slice(1);
    },
    //getDirName: function() { return global.appConfig.inst; },
    guid : function() {
        return 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    },
    passwd: function() {
        return 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    },
    resp:  resp,
    isUuid: function(uid) {
        return  uid.toString().match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i);
    },
    // Log
    log: {
        lg:     function () { doLog(arguments, '\x1b[32m'); },
        dbg:    function () { doLog(arguments, '\x1b[36m'); },
        warn:   function () { doLog(arguments, '\x1b[33m'); },
        info:   function () { doLog(arguments, '\x1b[34m'); },
        err:    function () { doLog(arguments, '\x1b[31m'); },
        glog:   function (arg) { console.log(c.green,   arg, c.end); },
        rlog:   function (arg) { console.log(c.red,     arg, c.end); },
        blog:   function (arg) { console.log(c.blue,    arg, c.end); },
        plog:   function (arg) { console.log(c.purple,  arg, c.end); },
        heart:  function (s) { logHeart(s)           }
    }, 

    glog: (arg, lab='') => console.log((lab ? lab :''), c.green,  arg, c.end),
    rlog: (arg, lab='') => console.log((lab ? lab :''), c.red,    arg, c.end),
    plog: (arg, lab='') => console.log((lab ? lab :''), c.purple, arg, c.end),
    blog: (arg, lab='') => console.log((lab ? lab :''), c.blue,   arg, c.end),
    brlog: (arg, lab='') => console.log((lab ? lab :''), c.brown,   arg, c.end),

    valid : {
        email: function(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        uid: function(uid) {
            return  uid.toString().match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i);
        }
    },
    getPromise: ( uri, auth ) => { 
        return new Promise( (y, n) =>
            require('request').get({ uri:  uri }, (err, rawResp, body) => 
                err ? n( body ) : y( body ) ) 
            )
    },
    //
    // callController - used in router files (./router/<controllerName>.js) to call controllers
    //
    //      Params: ctrl   - controller name. This function will call corresponding ./ctrls/<ctrl>.js
    //              action - action method to be called from the controller file
    //              params - params retrieved from the REST call and combined into one object
    //              res    - Server response object
    //              routeCallback - callback passed from an Express route to return API data
    //
    callController: (ctrl, action, params, res, routeCallback) => {
        console.log( 'ROUTER CALL ACTION:', ctrl, action);

        return require(`../ctrls/${ctrl}`)[action](params, (response, statusCode = 200) => {
            res.statusCode = statusCode;
            routeCallback( response );
        })
    },
    //
    // returnResult - called from controllers
    //                
    //      Params: data - returned from the controller
    //              success - true/false, (optional) 
    //              msg  - message string (optional), you can pass additional information
    //
    returnResult: (data, success = true, msg = '') => {
        typeof msg == 'undefined' && (msg = '');
        success || console.log('CONTROLLER ERROR', data);

        if (success) {
            return { "data": data, "success": success, "msg": msg };
        } else {
            return msg;
        }
    }
}

module.exports = me;
